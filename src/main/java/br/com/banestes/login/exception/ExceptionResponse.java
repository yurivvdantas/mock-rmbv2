package br.com.banestes.login.exception;

import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Exception customizada para ser padrão de resposta para qualquer exception
 * retornada na resposta da requsição
 * Autor: Yuri Dantas
 * Contato: yvdantas@banestes.com
 * #dontBeEvil
 */

@Data
@AllArgsConstructor
@ApiModel(description = "Formato de retorno de erro")
public class ExceptionResponse {
	@ApiModelProperty(notes = "Timestamp em que ocorreu o erro")
	private Date timestamp;
	@ApiModelProperty(notes = "Mensagem resumida do erro")
	private String mensagem;
	@ApiModelProperty(notes = "Mensagem completa do erro")
	private String detalhes;
}
