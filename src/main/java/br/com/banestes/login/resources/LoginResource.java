package br.com.banestes.login.resources;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.banestes.login.bo.LoginBO;
import br.com.banestes.login.dto.LoginContaRequest;
import br.com.banestes.login.dto.LoginContaResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;

/**
 * Resource criado para realizar o login simplificado Autor: Yuri Dantas
 * #dontBeEvil
 */
@RestController
@RequestMapping("/login/")
@Slf4j
@Api(value = "login", description = "Login simplificado API")
public class LoginResource {

    @Autowired
    private LoginBO loginBO;

    @ApiOperation(value = "login usando apenas conta e senha", nickname = "login", notes = "", response = LoginContaResponse.class, tags = {
            "login", })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Login com sucesso", response = LoginContaResponse.class),
            @ApiResponse(code = 400, message = "Dados inválidos"),
            @ApiResponse(code = 403, message = "Senha incorreta"),
            @ApiResponse(code = 404, message = "Conta não encontrada"),
            @ApiResponse(code = 405, message = "Operação inválida"),
            @ApiResponse(code = 500, message = "Ops, deu ruim e não sei como resolver."),
            @ApiResponse(code = 503, message = "Putz, não consigo resolver sua requisição no momento") })
    @RequestMapping(value = "/conta", produces = { "application/json" }, consumes = {
            "application/json" }, method = RequestMethod.POST)

    @PostMapping(path = "conta")
    public LoginContaResponse loginConta(
            @ApiParam(value = "Dados da conta que serão utilizados para o login", required = true) @Valid @RequestBody LoginContaRequest request) {
        log.info("Iniciado processo de login para os valores -> Conta: {}, senha: {}", request.getNumero(),
                request.getSenha());
        return loginBO.loginConta(request);

    }
}