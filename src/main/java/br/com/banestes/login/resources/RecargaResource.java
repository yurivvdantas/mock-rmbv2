package br.com.banestes.login.resources;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.banestes.login.dto.RecargaResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;

/**
 * Resource criado para realizar o mock da recarga de celular Autor: Yuri Dantas
 * #dontBeEvil
 */
@RestController
@RequestMapping("/recargaCelular/")
@Slf4j
@Api(value = "recargaCelular", description = "API responsável por realizar a recarga de créditos de celular")
public class RecargaResource {

        @ApiOperation(value = "Lista de operadoras e seus respectivos valores", nickname = "recargaCelular", notes = "", response = RecargaResponse.class, tags = {
                        "recargaCelular", })
        @ApiResponses(value = {
                        @ApiResponse(code = 200, message = "Lista de operadoras retornada com sucesso", response = RecargaResponse.class),
                        @ApiResponse(code = 400, message = "Dados inválidos"),
                        @ApiResponse(code = 405, message = "Operação inválida"),
                        @ApiResponse(code = 500, message = "Ops, deu ruim e não sei como resolver."),
                        @ApiResponse(code = 503, message = "Putz, não consigo resolver sua requisição no momento") })
        @RequestMapping(value = "/operadoras", produces = { "application/json" }, consumes = {
                        "application/json" }, method = RequestMethod.GET)

        @GetMapping(path = "operadoras")
        public RecargaResponse operadoras() {
                log.info("Iniciado processo de busca de operadoras para realizar a recarga");
                return new RecargaResponse();
        }
}