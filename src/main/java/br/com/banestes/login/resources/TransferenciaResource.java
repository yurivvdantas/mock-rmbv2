package br.com.banestes.login.resources;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.banestes.login.dto.TransferenciaResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;

/**
 * Resource criado para realizar o mock da transferência entre bancos
 * Autor: Yuri Dantas
 * #dontBeEvil
 */
@RestController
@Slf4j
@Api(value = "transferencia", description = "API responsável por realizar a transferência entre bancos")
public class TransferenciaResource {

        @ApiOperation(value = "Listas com as informações necessárias para preencher os dados de transferências entre bancos", 
                                nickname = "transferencia", notes = "", response = TransferenciaResponse.class, tags = {"transferencia", })
        @ApiResponses(value = {
                        @ApiResponse(code = 200, message = "Informações necessárias para permitir preencher dados de transferência", response = TransferenciaResponse.class),
                        @ApiResponse(code = 400, message = "Dados inválidos"),
                        @ApiResponse(code = 405, message = "Operação inválida"),
                        @ApiResponse(code = 500, message = "Ops, deu ruim e não sei como resolver."),
                        @ApiResponse(code = 503, message = "Putz, não consigo resolver sua requisição no momento") })
        @RequestMapping(value = "/transferencia", produces = { "application/json" }, consumes = {
                        "application/json" }, method = RequestMethod.GET)

        @GetMapping(path = "/transferencia/")
        public TransferenciaResponse transferenciaInfo() {
                log.info("Iniciado processo de buscar informações sobre transferências");
                return new TransferenciaResponse();
        }
}