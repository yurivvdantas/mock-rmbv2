package br.com.banestes.login.bo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.springframework.stereotype.Component;

import br.com.banestes.login.dto.ClienteDTO;
import br.com.banestes.login.dto.ContaDTO;
import br.com.banestes.login.dto.LoginContaRequest;
import br.com.banestes.login.dto.LoginContaResponse;
import br.com.banestes.login.enums.DispositivoSituacaoEnum;

@Component
public class LoginBO {

    public LoginContaResponse loginConta(LoginContaRequest contaRequest) {
        ContaDTO conta = new ContaDTO(contaRequest);
        ContaDTO contaCadastrada = getConta(conta.getNumero());
        
        // Erro 500 - Erro de tratamento da aplicação
        if (conta.getNumero().equals(0)) {
            throw new NullPointerException("Ops, deu ruim e não sei como resolver.");
        }

        // Erro 503 - Indisponibilidade momentânea 
        if (conta.getNumero().equals(1)) {
            throw new UnsupportedOperationException("Putz, não consigo resolver sua requisição no momento");
        }
        
        // conta não encontrada
        if (contaCadastrada == null) {
            throw new IllegalArgumentException("Conta não encontrada");
        }
        // senha incorreta
        if (!conta.getSenha().equals(contaCadastrada.getSenha())) {
            throw new SecurityException("Senha incorreta");
        }

        //login com sucesso
        return mockResponse(conta.getNumero());

    }

    private ContaDTO getConta(Integer numeroConta) {
        for (ContaDTO contaCadastrada : initMockConta()) {
            if (contaCadastrada.getNumero().equals(numeroConta))
                return contaCadastrada;
        }
        return null;
    }

    private LoginContaResponse mockResponse(Integer numeroConta) {
        List<ClienteDTO> listCliente = initMockClietes();
        
        Map<Integer, List<ClienteDTO>> mapContaCliente = new TreeMap<>();
        
        int i = 0;
        int sizeListCliente = listCliente.size();
        for (ContaDTO contas : initMockConta()) {
            ClienteDTO proxCliente = listCliente.get(i);
            i = incrementaI(i, sizeListCliente);
            List<ClienteDTO> listConta = new ArrayList<>(Arrays.asList(proxCliente));
            if(contas.getNumero()%2==0){
                listConta.add(listCliente.get(i));
                i = incrementaI(i, sizeListCliente);
            }
            mapContaCliente.put(contas.getNumero(), listConta);
        }
        
        return new LoginContaResponse(mapContaCliente.get(numeroConta),DispositivoSituacaoEnum.DESBLOQUEADO);
    }
    
    private List<ContaDTO> initMockConta() {
        ContaDTO contaDTO = new ContaDTO(1944818, "1234");
        ContaDTO contaDTO2 = new ContaDTO(363333, "1234");
        ContaDTO contaDTO3 = new ContaDTO(878470, "1234");
        ContaDTO contaDTO4 = new ContaDTO(975466, "1234");

        return new ArrayList<>(Arrays.asList(contaDTO, contaDTO2, contaDTO3, contaDTO4));
    }

    private List<ClienteDTO> initMockClietes() {
        Long id = 0l;
        ClienteDTO clienteDTO = new ClienteDTO(id++, "Eurico Miranda", mockCpf(id) ,"0-001");
        ClienteDTO clienteDTO2 = new ClienteDTO(id++, "Edson Arantes do Nascimento", mockCpf(id), "0-002");
        ClienteDTO clienteDTO3 = new ClienteDTO(id++, "Roberto Dinamite", mockCpf(id),"0-002");
        ClienteDTO clienteDTO4 = new ClienteDTO(id++, "Edmundo Animal", mockCpf(id),"0-004");
        ClienteDTO clienteDTO5 = new ClienteDTO(id++, "Romário da SIlva", mockCpf(id),"0-004");
        ClienteDTO clienteDTO6 = new ClienteDTO(id++, "Yago Pikachu", mockCpf(id),"0-005");
        ClienteDTO clienteDTO7 = new ClienteDTO(id++, "Jaiminho Carteiro",mockCpf(id),"0-005");
        ClienteDTO clienteDTO8 = new ClienteDTO(id++, "Seu Madruga",mockCpf(id), "0-006");

        return new ArrayList<>(Arrays.asList(clienteDTO,clienteDTO2,clienteDTO3,clienteDTO4,clienteDTO5,clienteDTO6,clienteDTO7,clienteDTO8));
    }

    private int incrementaI(int i, int sizeList) {
        int proxI = i + 1;
        if (sizeList == proxI)
            return 0;
        return proxI;
    }

    private String mockCpf(long id){
        List<String> listCfp = new ArrayList<>(Arrays.asList(
            "17334323025",
            "72465220004",
            "87344648083",
            "07045828055",
            "90790882000",
            "65964252079",
            "41052911064",
            "62579303081"));
        try{
            return listCfp.get((int) id - 1);
        }catch ( IndexOutOfBoundsException e){
            return "000000000000";
        }
    }
}