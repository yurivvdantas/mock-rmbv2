package br.com.banestes.login.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/*
DTO Responsável por retornar listas com informações necessárias para realizar transferências 
para recarga
*/
@Data
@NoArgsConstructor
@ApiModel(description = "Responsável por retornar listas com informações necessárias para realizar transferências")
public class TransferenciaResponse {
    @ApiModelProperty(notes = "Lista de bancos possíveis para realizar transferências")
    private List<Banco> listaBancos = mockBancos();

    @ApiModelProperty(notes = "Tipos de contas que são permitidas realizar transferências para outros bancos")
    private List<TipoContaGeral> listaTipoContaGeral = mockTiposContasGerais();

    @ApiModelProperty(notes = "Tipos de contas que são permitidas realizar transferências para o Banestes")
    private List<TipoContaBanestes> listaTipoContaBanestes = mockTiposContasBanestes();

    @ApiModelProperty(notes = "Tipos de transferências possíveis")
    private List<TipoTransferencia> listaTipoTransferencia = mockTipoTransferencia();

    @ApiModelProperty(notes = "Mensagens informativas a serem apresentadas ao usuário")
    private Map<String,String> listaMensagem = mockMensagem();

    private List<TipoContaGeral> mockTiposContasGerais(){
        List<TipoContaGeral> listTipoContaGeral = new ArrayList<>();
        listTipoContaGeral.add(new TipoContaGeral(1,"Conta Corrente"));
        listTipoContaGeral.add(new TipoContaGeral(2,"Conta Poupança"));
        listTipoContaGeral.add(new TipoContaGeral(3,"Conta Pagamento"));
        listTipoContaGeral.add(new TipoContaGeral(4,"Conta Jurídica"));
        return listTipoContaGeral;
    }
    
    private Map<String, String> mockMensagem() {
         Map<String,String> mensagens = new TreeMap<>();
         mensagens.put("mensagemInfoTedDoc","A Transferência Eletrônica Disponível (TED) não possui valor máximo para o envio de valores e a operação é concluída no mesmo dia se for realizada até às 17h. O Documento de Crédito (DOC) é  limitada a R$ 4.999,99 para o envio e leva um dia útil para concluir a operação, podendo ser cancelada nesse período.");
         return mensagens;
    }

    private List<TipoContaBanestes> mockTiposContasBanestes() {
        List<TipoContaBanestes> listTipoContaBanestes = new ArrayList<>();
        listTipoContaBanestes.add(new TipoContaBanestes(1,"Conta Corrente"));
        listTipoContaBanestes.add(new TipoContaBanestes(2,"Conta Poupança"));
        return listTipoContaBanestes;
    }
    
    private List<TipoTransferencia> mockTipoTransferencia(){
        List<TipoTransferencia> listTipoTransferencia = new ArrayList<>();
        listTipoTransferencia.add(new TipoTransferencia(1,"TED",mockFinalidadeTed(), 2));
        listTipoTransferencia.add(new TipoTransferencia(2,"DOC",new ArrayList<Finalidade>(),null));
        return listTipoTransferencia;
    }
    
    @AllArgsConstructor
    @Data
    private class Banco{
        private Integer id;
        private String codigoBanco;
        private String nome;
        private Boolean principal = null;
    }
    
    @AllArgsConstructor
    @Data
    private class TipoContaGeral{
        private Integer id;
        private String nome;
    }
    
    @AllArgsConstructor
    @Data
    private class TipoContaBanestes{
        private Integer id;
        private String nome;
    }
    
    @AllArgsConstructor
    @Data
    private class TipoTransferencia{
        private Integer id;
        private String nome;
        private List<Finalidade> listFinalidades;
        private Integer finalidadeDefault = null;
    }
    
    @AllArgsConstructor
    @Data
    private class Finalidade{
        private Integer id;
        private String nome;
    }
    
    
    private List<Finalidade> mockFinalidadeTed(){
        int i = 2;
        List<Finalidade> listFinalidade = new ArrayList<>();
        listFinalidade.add(new Finalidade(++i,"Crédito em conta"));
        listFinalidade.add(new Finalidade(++i,"Pagamento de Impostos, Tributos e Taxas"));
        listFinalidade.add(new Finalidade(++i,"Pagamento à Concessionárias de Serviço Público"));
        listFinalidade.add(new Finalidade(++i,"Pagamento de Dividendos"));
        listFinalidade.add(new Finalidade(++i,"Pagamento de Salários"));
        listFinalidade.add(new Finalidade(++i,"Pagamento de Fornecedores"));
        listFinalidade.add(new Finalidade(++i,"Pagamento de Honorários"));
        listFinalidade.add(new Finalidade(++i,"Pagamento de Aluguéis e Taxas de Condomínio"));
        listFinalidade.add(new Finalidade(++i,"Pagamento de Duplicatas e Títulos"));
        listFinalidade.add(new Finalidade(++i,"Pagamento de Mensalidade Escolar"));
        listFinalidade.add(new Finalidade(++i,"Pagamento a Corretoras"));
        listFinalidade.add(new Finalidade(++i,"Pagamento de Boleto Bancário em Cartório"));
        listFinalidade.add(new Finalidade(++i,"Pagamento de Tarifas pela Prestação de Serviços de Arrecadação de Convênios"));
        listFinalidade.add(new Finalidade(++i,"Repasse de Valores Referentes a Títulos Liquidados em Cartórios de Protesto"));
        listFinalidade.add(new Finalidade(++i,"Liquidação Financeira de Operadora de Cartão"));
        listFinalidade.add(new Finalidade(++i,"Crédito em Conta de Investimento Mantida na IF Destinatária"));
        listFinalidade.add(new Finalidade(++i,"Crédito em Conta de Investimento Mantida em Cliente da IF Destinatária"));
        listFinalidade.add(new Finalidade(++i,"Operações Seguro Habitacional - SFH"));
        listFinalidade.add(new Finalidade(++i,"Operações do FDS - Caixa"));
        listFinalidade.add(new Finalidade(++i,"Pagamento de Operações de Crédito"));
        listFinalidade.add(new Finalidade(++i,"Crédito para Investimento - Conta de Investimento Mantida na IF Origem"));
        listFinalidade.add(new Finalidade(++i,"Crédito para Investimento - Conta de Investimento Mantida em Cliente da IF Origem"));
        listFinalidade.add(new Finalidade(++i,"Taxa de Administração"));
        listFinalidade.add(new Finalidade(++i,"Crédito para Aplicação em Cliente da IF Destinatária - Conta de Investimento Mantida na "));
        listFinalidade.add(new Finalidade(++i,"Crédito para Aplicação em Cliente da IF Destinatária - Conta de Investimento Mantida "));
        listFinalidade.add(new Finalidade(++i,"Crédito em CI Mantida na IF Destinatária Proveniente de IF sem Reservas Bancárias"));
        listFinalidade.add(new Finalidade(++i,"Pagamento de Acordo / Execução Judicial"));
        listFinalidade.add(new Finalidade(++i,"Liquidação de Empréstimos Consignados"));
        listFinalidade.add(new Finalidade(++i,"Pagamento de Bolsa Auxílio"));
        listFinalidade.add(new Finalidade(++i,"Remuneração à Cooperado"));
        listFinalidade.add(new Finalidade(++i,"Pagamento de Prebenda (Remuneração a Padres e Sacerdotes)"));
        listFinalidade.add(new Finalidade(++i,"Retirada de Recursos da Conta de Investimento"));
        listFinalidade.add(new Finalidade(++i,"Pagamento de Juros sobre Capital Próprio"));
        listFinalidade.add(new Finalidade(++i,"Pagamento de Rendimentos ou Amortização s/ Cotas e/ou Debêntures"));
        listFinalidade.add(new Finalidade(++i,"Taxa de Serviço"));
        listFinalidade.add(new Finalidade(++i,"Pagamento de Juros e/ou Amortização de Títulos Depositados em Garantia nas Câmaras"));
        listFinalidade.add(new Finalidade(++i,"Estorno ou Restituição – Diversos"));
        listFinalidade.add(new Finalidade(++i,"Pagamento de Vale Transporte"));
        listFinalidade.add(new Finalidade(++i,"Simples Nacional"));
        listFinalidade.add(new Finalidade(++i,"Repasse de Valores para o FUNDEB"));
        listFinalidade.add(new Finalidade(++i,"Repasse de Valores de Convênio Centralizado"));
        listFinalidade.add(new Finalidade(++i,"Patrocínio com Incentivo Fiscal"));
        listFinalidade.add(new Finalidade(++i,"Doação com Incentivo Fiscal"));
        listFinalidade.add(new Finalidade(++i,"Transferência de Conta Corrente de Instituição Não Bancária para sua Conta de "));
        listFinalidade.add(new Finalidade(++i,"Pagamento de Rescisão de Contrato de Trabalho"));
        listFinalidade.add(new Finalidade(++i,"Reembolso de Despesas com Estruturação de Operações de Renda Fixa e Variável"));
        listFinalidade.add(new Finalidade(++i,"Compra de Moeda Estrangeira pelo FSB - Fundo Soberano do Brasil"));
        listFinalidade.add(new Finalidade(++i,"Depósito Judicial"));
        listFinalidade.add(new Finalidade(++i,"Pensão Alimentícia"));
        listFinalidade.add(new Finalidade(++i,"Cessão de Créditos - Liquidação Principal por Aquisição de Créditos ou Direitos Creditórios ou Fluxo de Caixa Garantido por Créditos, por Ordem de Cliente PJ Financeira"));
        listFinalidade.add(new Finalidade(++i,"Cessão de Créditos - Liquidação Principal por Aquisição de Créditos ou Direitos Creditórios, por Ordem de FIDC ou Cia Securitizadora"));
        listFinalidade.add(new Finalidade(++i,"Cessão de Créditos - Repasse Contratual de Fluxo de Caixa ou de Recebíveis Pagos, por "));
        listFinalidade.add(new Finalidade(++i,"Cessão de Créditos - Repasse Contratual de Fluxo de Caixa ou de Recebíveis Pagos Antecipadamente, por Ordem de Cliente PJ Financeira"));
        listFinalidade.add(new Finalidade(++i,"Cessão de Créditos - Ajustes Diversos"));
        listFinalidade.add(new Finalidade(++i,"Transferência entre Contas de Mesma Titularidade"));
        listFinalidade.add(new Finalidade(++i,"Crédito para Investidor em Cliente da IF Creditada"));
        listFinalidade.add(new Finalidade(++i,"Débito de Investidor em Cliente da IF Debitada"));
        listFinalidade.add(new Finalidade(++i,"Pagamento de Operações de Crédito por Cliente"));
        listFinalidade.add(new Finalidade(++i,"Resgate de Aplicação Financeira de Cliente para Conta de sua Titularidade"));
        listFinalidade.add(new Finalidade(++i,"Aplicação Financeira em Nome do Cliente Remetente"));
        listFinalidade.add(new Finalidade(++i,"Cessão de Créditos - Liquidação Principal por Recompra de Créditos ou Direitos Creditórios ou Fluxo de Caixa Garantido por Créditos, por Ordem de Cliente PJ Financeira"));
        listFinalidade.add(new Finalidade(++i,"Cessão de Créditos - Liquidação Principal por Recompra de Créditos ou Direitos Creditórios, por Ordem de FIDC ou Cia Securitizadora"));
        listFinalidade.add(new Finalidade(++i,"FGCoop - Recolhimento Fundo Garantidor do Cooperativismo de Crédito"));
        listFinalidade.add(new Finalidade(++i,"FGCoop - Devolução de Recolhimento a Maior"));
        listFinalidade.add(new Finalidade(++i,"Crédito ao Consumidor Decorrente de Programa de Incentivo Fiscal"));
        listFinalidade.add(new Finalidade(++i,"Transferência Internacional em Reais"));
        listFinalidade.add(new Finalidade(++i,"Ajuste Posição Mercado Futuro"));
        listFinalidade.add(new Finalidade(++i,"Repasse de Valores do BNDES"));
        listFinalidade.add(new Finalidade(++i,"Liquidação de Compromissos Junto ao BNDES"));
        listFinalidade.add(new Finalidade(++i,"Operações de Compra e Venda de Ações - Bolsas de Valores e Mercado de Balcão"));
        listFinalidade.add(new Finalidade(++i,"Contratos Referenciados em Ações ou Índices de Ações - Bolsas de Valores, de "));
        listFinalidade.add(new Finalidade(++i,"Operação de Câmbio - Não Interbancária"));
        listFinalidade.add(new Finalidade(++i,"Operações no Mercado de Renda Fixa e de Renda Variável com Utilização de "));
        listFinalidade.add(new Finalidade(++i,"Operação de Câmbio - Mercado Interbancário - Instituições sem Conta Reservas "));
        listFinalidade.add(new Finalidade(++i,"Pagamento de Operações com Identificação de Destinatário Final"));
        listFinalidade.add(new Finalidade(++i,"Restituição de Imposto de Renda"));
        listFinalidade.add(new Finalidade(++i,"Ordem Bancária do Tesouro – OBT"));
        listFinalidade.add(new Finalidade(++i,"Pagamento de Multas ao BACEN por Atrasos de Importação"));
        listFinalidade.add(new Finalidade(++i,"Restituição de Tributos – RFB"));
        listFinalidade.add(new Finalidade(++i,"TEA - Transferência Eletrônica Agendada"));
        listFinalidade.add(new Finalidade(++i,"Restituição de Prêmios de Seguros"));
        listFinalidade.add(new Finalidade(++i,"Pagamento de Indenização de Sinistro de Seguro"));
        listFinalidade.add(new Finalidade(++i,"Pagamento de Prêmio de Co-Seguro"));
        listFinalidade.add(new Finalidade(++i,"Restituição de Prêmio de Co-Seguro"));
        listFinalidade.add(new Finalidade(++i,"Pagamento de Indenização de Sinistro de Co-Seguro"));
        listFinalidade.add(new Finalidade(++i,"Pagamento de Prêmio de Resseguro"));
        listFinalidade.add(new Finalidade(++i,"Restituição de Prêmio de Resseguro"));
        listFinalidade.add(new Finalidade(++i,"Pagamento de Indenização de Sinistro de Resseguro"));
        listFinalidade.add(new Finalidade(++i,"Restituição de Indenização de Sinistro de Resseguro"));
        listFinalidade.add(new Finalidade(++i,"Pagamento de Despesas com Sinistros"));
        listFinalidade.add(new Finalidade(++i,"Pagamento de Inspeções/Vistorias Prévias"));
        listFinalidade.add(new Finalidade(++i,"Pagamento de Resgate de Título da Capitalização"));
        listFinalidade.add(new Finalidade(++i,"Pagamento de Sorteio de Título de Capitalização"));
        listFinalidade.add(new Finalidade(++i,"Pagamento de Devolução de Mensalidade de Título de Capitalização"));
        listFinalidade.add(new Finalidade(++i,"Restituição de Contribuição de Plano Previdenciário"));
        listFinalidade.add(new Finalidade(++i,"Pagamento de Benefício Previdenciário de Pecúlio"));
        listFinalidade.add(new Finalidade(++i,"Pagamento de Benefício Previdenciário de Pensão"));
        listFinalidade.add(new Finalidade(++i,"Pagamento de Benefício Previdenciário de Aposentadoria"));
        listFinalidade.add(new Finalidade(++i,"Pagamento de Resgate Previdenciário"));
        listFinalidade.add(new Finalidade(++i,"Pagamento de Comissão de Corretagem"));
        listFinalidade.add(new Finalidade(++i,"Pagamento de Transferências/Portabilidade de Reserva de Seguro/Previdência"));
        listFinalidade.add(new Finalidade(++i,"Outros"));
        return listFinalidade;
    }

    private List<Banco> mockBancos(){
        int i = 1;
        List<Banco> listBancos = new ArrayList<>();
        listBancos.add(new Banco(i,"021", "Banestes", true));
        
        listBancos.add(new Banco(++i,"001", "Banco do Brasil S.A.", true));
        listBancos.add(new Banco(++i,"341", "Banco Itaú S.A.", true));
        listBancos.add(new Banco(++i,"033", "Banco Santander (Brasil) S.A.", true));
        listBancos.add(new Banco(++i,"237", "Banco Bradesco S.A.", true));
        listBancos.add(new Banco(++i,"104", "Caixa Econômica Federal", true));
        
        listBancos.add(new Banco(++i,"246", "Banco ABC Brasil S.A.", null));
        listBancos.add(new Banco(++i,"025", "Banco Alfa S.A.", null));
        listBancos.add(new Banco(++i,"641", "Banco Alvorada S.A.", null));
        listBancos.add(new Banco(++i,"029", "Banco Banerj S.A.", null));
        listBancos.add(new Banco(++i,"038", "Banco Banestado S.A.", null));
        listBancos.add(new Banco(++i,"000", "Banco Bankpar S.A.", null));
        listBancos.add(new Banco(++i,"740", "Banco Barclays S.A.", null));
        listBancos.add(new Banco(++i,"107", "Banco BBM S.A.", null));
        listBancos.add(new Banco(++i,"031", "Banco Beg S.A.", null));
        listBancos.add(new Banco(++i,"096", "Banco BM&F de Serviços de Liquidação e Custódia S.A", null));
        listBancos.add(new Banco(++i,"318", "Banco BMG S.A.", null));
        listBancos.add(new Banco(++i,"752", "Banco BNP Paribas Brasil S.A.", null));
        listBancos.add(new Banco(++i,"248", "Banco Boavista Interatlântico S.A.", null));
        listBancos.add(new Banco(++i,"036", "Banco Bradesco BBI S.A.", null));
        listBancos.add(new Banco(++i,"204", "Banco Bradesco Cartões S.A.", null));
        listBancos.add(new Banco(++i,"225", "Banco Brascan S.A.", null));
        listBancos.add(new Banco(++i,"044", "Banco BVA S.A.", null));
        listBancos.add(new Banco(++i,"263", "Banco Cacique S.A.", null));
        listBancos.add(new Banco(++i,"473", "Banco Caixa Geral - Brasil S.A.", null));
        listBancos.add(new Banco(++i,"222", "Banco Calyon Brasil S.A.", null));
        listBancos.add(new Banco(++i,"040", "Banco Cargill S.A.", null));
        listBancos.add(new Banco(++i,"M08", "Banco Citicard S.A.", null));
        listBancos.add(new Banco(++i,"M19", "Banco CNH Capital S.A.", null));
        listBancos.add(new Banco(++i,"215", "Banco Comercial e de Investimento Sudameris S.A.", null));
        listBancos.add(new Banco(++i,"756", "Banco Cooperativo do Brasil S.A. - BANCOOB", null));
        listBancos.add(new Banco(++i,"748", "Banco Cooperativo Sicredi S.A.", null));
        listBancos.add(new Banco(++i,"505", "Banco Credit Suisse (Brasil) S.A.", null));
        listBancos.add(new Banco(++i,"229", "Banco Cruzeiro do Sul S.A.", null));
        listBancos.add(new Banco(++i,"003", "Banco da Amazônia S.A.", null));
        listBancos.add(new Banco(++i,"083-3", "Banco da China Brasil S.A.", null));
        listBancos.add(new Banco(++i,"707", "Banco Daycoval S.A.", null));
        listBancos.add(new Banco(++i,"M06", "Banco de Lage Landen Brasil S.A.", null));
        listBancos.add(new Banco(++i,"024", "Banco de Pernambuco S.A.- BANDEPE", null));
        listBancos.add(new Banco(++i,"456", "Banco de Tokyo-Mitsubishi UFJ Brasil S.A.", null));
        listBancos.add(new Banco(++i,"214", "Banco Dibens S.A.", null));
        listBancos.add(new Banco(++i,"047", "Banco do Estado de Sergipe S.A.", null));
        listBancos.add(new Banco(++i,"037", "Banco do Estado do Pará S.A.", null));
        listBancos.add(new Banco(++i,"041", "Banco do Estado do Rio Grande do Sul S.A.", null));
        listBancos.add(new Banco(++i,"004", "Banco do Nordeste do Brasil S.A.", null));
        listBancos.add(new Banco(++i,"265", "Banco Fator S.A.", null));
        listBancos.add(new Banco(++i,"M03", "Banco Fiat S.A.", null));
        listBancos.add(new Banco(++i,"224", "Banco Fibra S.A.", null));
        listBancos.add(new Banco(++i,"626", "Banco Ficsa S.A.", null));
        listBancos.add(new Banco(++i,"394", "Banco Finasa BMC S.A.", null));
        listBancos.add(new Banco(++i,"M18", "Banco Ford S.A.", null));
        listBancos.add(new Banco(++i,"233", "Banco GE Capital S.A.", null));
        listBancos.add(new Banco(++i,"734", "Banco Gerdau S.A.", null));
        listBancos.add(new Banco(++i,"M07", "Banco GMAC S.A.", null));
        listBancos.add(new Banco(++i,"612", "Banco Guanabara S.A.", null));
        listBancos.add(new Banco(++i,"M22", "Banco Honda S.A.", null));
        listBancos.add(new Banco(++i,"063", "Banco Ibi S.A. Banco Múltiplo", null));
        listBancos.add(new Banco(++i,"M11", "Banco IBM S.A.", null));
        listBancos.add(new Banco(++i,"604", "Banco Industrial do Brasil S.A.", null));
        listBancos.add(new Banco(++i,"320", "Banco Industrial e Comercial S.A.", null));
        listBancos.add(new Banco(++i,"653", "Banco Indusval S.A.", null));
        listBancos.add(new Banco(++i,"630", "Banco Intercap S.A.", null));
        listBancos.add(new Banco(++i,"249", "Banco Investcred Unibanco S.A.", null));
        listBancos.add(new Banco(++i,"184", "Banco Itaú BBA S.A.", null));
        listBancos.add(new Banco(++i,"479", "Banco ItaúBank S.A", null));
        listBancos.add(new Banco(++i,"M09", "Banco Itaucred Financiamentos S.A.", null));
        listBancos.add(new Banco(++i,"376", "Banco J. P. Morgan S.A.", null));
        listBancos.add(new Banco(++i,"074", "Banco J. Safra S.A.", null));
        listBancos.add(new Banco(++i,"217", "Banco John Deere S.A.", null));
        listBancos.add(new Banco(++i,"065", "Banco Lemon S.A.", null));
        listBancos.add(new Banco(++i,"600", "Banco Luso Brasileiro S.A.", null));
        listBancos.add(new Banco(++i,"755", "Banco Merrill Lynch de Investimentos S.A.", null));
        listBancos.add(new Banco(++i,"746", "Banco Modal S.A.", null));
        listBancos.add(new Banco(++i,"151", "Banco Nossa Caixa S.A.", null));
        listBancos.add(new Banco(++i,"045", "Banco Opportunity S.A.", null));
        listBancos.add(new Banco(++i,"623", "Banco Panamericano S.A.", null));
        listBancos.add(new Banco(++i,"611", "Banco Paulista S.A.", null));
        listBancos.add(new Banco(++i,"643", "Banco Pine S.A.", null));
        listBancos.add(new Banco(++i,"638", "Banco Prosper S.A.", null));
        listBancos.add(new Banco(++i,"747", "Banco Rabobank International Brasil S.A.", null));
        listBancos.add(new Banco(++i,"M16", "Banco Rodobens S.A.", null));
        listBancos.add(new Banco(++i,"072", "Banco Rural Mais S.A.", null));
        listBancos.add(new Banco(++i,"250", "Banco Schahin S.A.", null));
        listBancos.add(new Banco(++i,"749", "Banco Simples S.A.", null));
        listBancos.add(new Banco(++i,"366", "Banco Société Générale Brasil S.A.", null));
        listBancos.add(new Banco(++i,"637", "Banco Sofisa S.A.", null));
        listBancos.add(new Banco(++i,"464", "Banco Sumitomo Mitsui Brasileiro S.A.", null));
        listBancos.add(new Banco(++i,"082-5", "Banco Topázio S.A.", null));
        listBancos.add(new Banco(++i,"M20", "Banco Toyota do Brasil S.A.", null));
        listBancos.add(new Banco(++i,"634", "Banco Triângulo S.A.", null));
        listBancos.add(new Banco(++i,"208", "Banco UBS Pactual S.A.", null));
        listBancos.add(new Banco(++i,"M14", "Banco Volkswagen S.A.", null));
        listBancos.add(new Banco(++i,"655", "Banco Votorantim S.A.", null));
        listBancos.add(new Banco(++i,"610", "Banco VR S.A.", null));
        listBancos.add(new Banco(++i,"370", "Banco WestLB do Brasil S.A.", null));
        listBancos.add(new Banco(++i,"719", "Banif-Banco Internacional do Funchal (Brasil)S.A.", null));
        listBancos.add(new Banco(++i,"073", "BB Banco Popular do Brasil S.A.", null));
        listBancos.add(new Banco(++i,"078", "BES Investimento do Brasil S.A.-Banco de Investimento", null));
        listBancos.add(new Banco(++i,"069", "BPN Brasil Banco Múltiplo S.A.", null));
        listBancos.add(new Banco(++i,"070", "BRB - Banco de Brasília S.A.", null));
        listBancos.add(new Banco(++i,"477", "Citibank N.A.", null));
        listBancos.add(new Banco(++i,"081-7", "Concórdia Banco S.A.", null));
        listBancos.add(new Banco(++i,"487", "Deutsche Bank S.A. - Banco Alemão", null));
        listBancos.add(new Banco(++i,"751", "Dresdner Bank Brasil S.A.- Banco Múltiplo", null));
        listBancos.add(new Banco(++i,"062", "Hipercard Banco Múltiplo S.A.", null));
        listBancos.add(new Banco(++i,"492", "ING Bank N.V.", null));
        listBancos.add(new Banco(++i,"488", "JPMorgan Chase Bank", null));
        listBancos.add(new Banco(++i,"409", "UNIBANCO - União de Bancos Brasileiros S.A.", null));
        listBancos.add(new Banco(++i,"230", "Unicard Banco Múltiplo S.A.", null));
        return listBancos;
    }
}

