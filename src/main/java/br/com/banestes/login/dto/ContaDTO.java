package br.com.banestes.login.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

/*
*DTO Responsável por transitar dados da conta
*/ 
@Data
@ApiModel(description = "Responsável por transitar dados da conta")
@AllArgsConstructor
public class ContaDTO{
    @ApiModelProperty(notes = "Numero da conta")
    private Integer numero;
    @ApiModelProperty(notes = "Senha no momento não encriptada da conta")
    private String senha;
    
    public ContaDTO(LoginContaRequest loginContaRequest){
        this.numero = loginContaRequest.getNumero();
        this.senha = loginContaRequest.getSenha();
    }
}