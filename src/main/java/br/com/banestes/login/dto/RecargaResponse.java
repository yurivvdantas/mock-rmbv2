package br.com.banestes.login.dto;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/*
DTO Responsável por retornar map com a lista de operadoras e seus respectivos valores permitidos 
para recarga
*/
@Data
@NoArgsConstructor
@ApiModel(description = "Responsável por retornar map com a lista de operadoras e seus respectivos valores permitidos para recarga")
public class RecargaResponse {
    @ApiModelProperty(notes = "Map com operadoras e seus respectivos valores")
    private Map<String,Valores> operadoras = mockOperadoras();

    private Map<String,Valores> mockOperadoras(){
        Map<String,Valores> mapOperadoras = new TreeMap<>();
        mapOperadoras.put("Vivo", new Valores(new ArrayList<>(Arrays.asList(5,10,15,35))));
        mapOperadoras.put("Oi", new Valores(new ArrayList<>(Arrays.asList(5,7,17))));
        mapOperadoras.put("Tim", new Valores(new ArrayList<>(Arrays.asList(1,13,6,135,1335))));
        mapOperadoras.put("Claro", new Valores(new ArrayList<>(Arrays.asList(4,10,35,75))));
        mapOperadoras.put("Nextel", new Valores(new ArrayList<>(Arrays.asList(5,10,15,35,70,95,105,115))));
        return mapOperadoras;
    }

    @AllArgsConstructor
    @Data
    private class Valores{
        @ApiModelProperty(notes = "Valores possíveis de realizar uma recarga")
        private List<Integer> valores;
    }
}