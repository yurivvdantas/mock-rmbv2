package br.com.banestes.login.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

/*
*DTO responsável por retornar dados vinculados ao cliente
*/ 
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@AllArgsConstructor
@ApiModel(description = "Responsavel por transitar dados do cliente da conta")
public class ClienteDTO{
    @ApiModelProperty(notes = "Identificador do cliente")
    private Long id;
    @ApiModelProperty(notes = "Nome do cliente")
    private String nome;
    @ApiModelProperty(notes = "Cpf do cliente")
    private String cpf;
    @ApiModelProperty(notes = "Agência da conta do cliente")
    private String agencia;
}