package br.com.banestes.login.dto;

import java.util.List;

import br.com.banestes.login.enums.DispositivoSituacaoEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

/*
*DTO Responsável por retornar dados no login
*/ 
@Data
@AllArgsConstructor
@ApiModel(description = "Responsável por retornar dados no login")
public class LoginContaResponse{
    @ApiModelProperty(notes = "Lista de clientes da conta logada")
    private List<ClienteDTO> clientes;
    @ApiModelProperty(notes = "Status do dispositivo logado")
    private DispositivoSituacaoEnum statusDispositivo;
}