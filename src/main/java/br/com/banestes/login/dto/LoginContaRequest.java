package br.com.banestes.login.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/*
*DTO Responsável por receber o request com os dados da conta
*/ 
@Data
@ApiModel(description = "Responsável por receber o request com os dados da conta")
public class LoginContaRequest{
    @ApiModelProperty(notes = "Numero da conta")
    private Integer numero;
    @ApiModelProperty(notes = "Senha no momento não encriptada da conta")
    private String senha;
}