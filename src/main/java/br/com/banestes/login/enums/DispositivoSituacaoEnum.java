package br.com.banestes.login.enums;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

/**
 * Autor: Yuri Dantas Contato: yvdantas@banestes.com 
 * Descrição: Enum criado para mapear os status possíveis do dispositivo
 * #dontBeEvil
 */

@Getter
@AllArgsConstructor
@ToString
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
@ApiModel(description = "Status possíveis para o dispositivo")
public enum DispositivoSituacaoEnum {
    DESBLOQUEADO(1, "Desbloqueado"),
	BLOQUEADO_BANESTES(2, "Bloqueado pelo Banestes"),
	BLOQUEADO_CLIENTE(3, "Bloqueado pelo Cliente"),
	DESBLOQUEIO_SOLICITADO(4, "Desbloqueio solicitado"),
	DESBLOQUEIO_NAO_AUTORIZADO(5, "Desbloqueio não autorizado");

	private int codigo;
	private String descricao;

}