# VALIDACAO-ABRE-CONTA
Sistema responsável por validar regras de negócio específicas do app abre conta e que condicionam a abertura da conta.

## Getting Started

### Instalação

Clone o projeto do bitbucket:
```
git clone ssh://git@p0lvap01s59.banestes.sfb:7999/dcd/validacao-abre-conta.git
```

Execute o build usando o maven através do comando:
```
mvn install
```

## Changelog

### [1.0.0] - 29-01-2020
- Adicionado método que verifica se determinado CEP está apto a abrir conta;

## Autor

Yuri Vinturini Vieira Dantas (yvdantas@banestes.com.br)

#dontBeEvil

-> ADD: Link do confluence com swagger;
        Adicionar info do openshift: Namespace, links de DEV, UAT e PRD;
        Adicionar forma de rodar aplicação;